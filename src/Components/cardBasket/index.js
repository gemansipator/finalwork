import "./cardBasket.css";

function cardBasket(props) {
  console.log(props);
  return (
    <div className="card_basket">
        <div className="card_basket_wrapper1">
          <img className="card__preview_basket" src={props.url} alt=""></img>
        </div>
        <div className="card_basket_wrapper2">
            <h2 className="card__title_basket">{props.title}</h2>
        </div>
        <div className="card_basket_wrapper3">
            <p className="card__price_basket">
                <button className="card__button_basket" onClick={props.add}>
                    +
                </button>
                 {props.quantity}шт.
                <button className="card__button_basket" onClick={props.remove}>
                    -
                </button>
                x {props.price} ₽ = {props.quantity * props.price} ₽
            </p>

        </div>
        <div className="card_basket_wrapper4">
            <button className="card__button_basket_b" onClick={props.removeAll}>
                удалить
            </button>
        </div>
    </div>
  );
}

export default cardBasket;
