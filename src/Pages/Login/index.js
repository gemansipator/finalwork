import React, { useState } from "react";
import "./login.scss";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { authLogin } from "../../store/reducers/auth";

export const Login = () => {
  const dispatch = useDispatch();

  let navigate = useNavigate();
  const routeChange = () => {
    let path = `/products`;
    navigate(path);
  };

  const [login, setLogin] = useState("Vasya");
  const [password, setPassword] = useState("12345");

  const handleClick = () => {
    dispatch(authLogin({ login: login, password: password }));
    routeChange();
  };

  return (
    <div className="wraper-container-login">
            <div className="container-login">
          <h1 className="container-login_title">
            Страница входа
          </h1>
          <input
            type="text"
            placeholder="Login"
            className="container-login__item"
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          ></input>
          <input
            type="password"
            placeholder="Password"
            className="container-login__item"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></input>
          <button className="container-login__item2" onClick={handleClick}>
            Вход
          </button>
        </div>
    </div>
  );
};
